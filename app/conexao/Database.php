<?php

require_once(__DIR__."/../config/database.php");

class Database{

	private static $connection= null;
	private static $instance = null;
	private static $host     = DATABASE_HOST;
	private static $username = DATABASE_USUARIO;
	private static $password = DATABASE_SENHA;
	private static $database = DATABASE_BANCO;


	public static function getInstance() {
		
		if(!self::$instance) { 
			self::$instance = new self();
		}
		return self::$instance;
	}
	private function __construct() {
		
		try{
		
			self::$connection = new PDO("mysql:host=".self::$host.";dbname=".self::$database,self::$username,self::$password);
		
		}catch(\Exception $e){
			echo $e->getMessage();
			exit;
		}catch(\Error $e){
			echo $e->getMessage();
			exit;
		}	

	}
	public static function getConexao() {
		return self::$connection;
	}
	public static function getParams(){

		return array(
			"host"     => self::$host,
			"username" => self::$username,
			"password" => self::$password,
			"database" => self::$database
		);

	}

}




?>