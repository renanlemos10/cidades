<?php

require_once(__DIR__."/../conexao/Database.php");

class Cidade extends Database{

	private static $table = "cidades";
	
	public static function all(){

		$Database = self::getInstance();

		$query  = "SELECT c.id,c.descricao,c.estado_id,e.descricao as estado FROM ".self::$table." as c";
		$query .= " INNER JOIN estados as e ON e.id = c.estado_id";
		$query .= " ORDER BY c.id desc";

		$data = $Database->getConexao()->query($query);

		return $data->fetchAll(PDO::FETCH_ASSOC);

	}
	public static function pesquisar($descricao){

		$Database = self::getInstance();

		$query  = "SELECT c.id,c.descricao,c.estado_id,e.descricao as estado FROM ".self::$table." as c";
		$query .= " INNER JOIN estados as e ON e.id = c.estado_id";
		$query .= " WHERE c.descricao like '%".$descricao."%'";

		$data = $Database->getConexao()->query($query);

		return $data->fetchAll(PDO::FETCH_ASSOC);

	} 
	public static function create($params){

		$Database = self::getInstance();

		$smtp = $Database->getConexao()->prepare("INSERT INTO ".self::$table." (descricao,estado_id) VALUES (:descricao,:estado_id)");

		return $smtp->execute($params);

	} 
	public static function update($params){

		$Database = self::getInstance();
		$campos   = "";
		$paramsOut = array();

		if(is_array($params)){
			
			foreach($params as $key => $value) {
			 	
			 	if($key != "id" and !is_null($value)){
			 		if(!empty($campos)){
			 			$campos .= ",";
			 		}
			 		$campos .= $key."=:".$key;
			 		$paramsOut[$key] = $value; 
			 	}else if($key == "id"){
			 		$paramsOut[$key] = $value; 
			 	}

			}

		}	

		$smtp = $Database->getConexao()->prepare("UPDATE ".self::$table." SET ".$campos." WHERE id=:id");

		return $smtp->execute($paramsOut);
		return $campos;

	} 
	public static function delete($id){

		$Database = self::getInstance();

		$smtp = $Database->getConexao()->prepare("DELETE FROM ".self::$table." WHERE id = :id");

		return $smtp->execute(array("id" => $id));

	} 



}

	


?>