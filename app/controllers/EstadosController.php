<?php

require_once(__DIR__."/../config/init.php");
require_once(__DIR__."/../models/Estado.php");

class EstadosController{


	public function all(){

		try{
			$response = array(
				"data" => Estado::all(),
				"status" => 1
			);
		}catch(\Exception $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}catch(\Error $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}
		header('Content-Type: application/json');
		echo json_encode($response);

	}


}

$action  = $_REQUEST["action"];
$Estados = new EstadosController();

switch($action){
	case 'all':
		$Estados->all();
	break;
	default:
		echo json_encode(array());
	break;
}


?>