<?php

require_once(__DIR__."/../config/init.php");
require_once(__DIR__."/../models/Cidade.php");

class CidadesController{

	private $method = null;
	private $params = null;

	public function __construct(){

		$postdata     = file_get_contents("php://input");
		$this->params = (array)json_decode($postdata);
		$this->method = $_SERVER['REQUEST_METHOD'];
	} 
	public function all(){

		try{
			$response = array(
				"data" => Cidade::all(),
				"status" => 1
			);
		}catch(\Exception $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}catch(\Error $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}
		header('Content-Type: application/json');
		echo json_encode($response);

	}
	public function pesquisar(){

		try{
			
			$descricao  = isset($_REQUEST["descricao"]) ? $_REQUEST["descricao"] : "";

			$response = array(
				"data" => Cidade::pesquisar($descricao),
				"status" => 1
			);

		}catch(\Exception $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}catch(\Error $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}
		header('Content-Type: application/json');
		echo json_encode($response);

	}
	public function create(){

		try{
			
			$validate = $this->validate();

			if(count($validate) == 0){
			
				$data 			   = array();
				$data["descricao"] = isset($this->params["descricao"]) ? $this->params["descricao"] : null;
				$data["estado_id"] = isset($this->params["estado_id"]) ? (int)$this->params["estado_id"] : null;
				
				$response = array(
					"data"   => Cidade::create($data),
					"status" => 1
				);

			}else{

				$response = array(
					"data"   => $validate,
					"status" => 0
				);

			}	

		}catch(\Exception $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}catch(\Error $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}
		header('Content-Type: application/json');
		echo json_encode($response);

	}
	public function update(){

		try{
			
			$validate = $this->validate();

			if(count($validate) == 0){
			
				$params = $this->params;

				$data = array();
				$data["id"] = isset($params["id"]) ? $params["id"] : null;
				$data["descricao"] = isset($params["descricao"]) ? $params["descricao"] : null;
				$data["estado_id"] = isset($params["estado_id"]) ? (int)$params["estado_id"] : null;

				$response = array(
					"data"   => Cidade::update($data),
					"status" => 1
				);

			}else{

				$response = array(
					"data"   => $validate,
					"status" => 0
				);

			}	

		}catch(\Exception $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}catch(\Error $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}
		header('Content-Type: application/json');
		echo json_encode($response);

	}
	public function delete(){

		try{

			$id = isset($this->params["id"]) ? (int)$this->params["id"] : $_REQUEST["id"];

			$response = array(
				"data"   => Cidade::delete($id),
				"status" => 1
			);

		}catch(\Exception $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}catch(\Error $e){
			$response = array("data" => $e->getMessage(),"status" => 500);
		}

		header('Content-Type: application/json');
		echo json_encode($response);

	}
	public function validate(){

		$params = $this->params;
		$errors = array();

		$estado_id = isset($params["estado_id"]) ? $params["estado_id"] : "";
		$descricao = isset($params["descricao"]) ? $params["descricao"] : "";

		if(empty($params["descricao"])){

			$errors["cidade"] = "- Informe a Cidade";
		}
		if(empty($params["estado_id"])){
				
			$errors["estado"] = "- Informe o Estado";

		}

		return $errors;


	}
	public function getMethod(){
		return $this->method;
	}
	public function checkMethod($method){

		if($method != $this->getMethod()){
			header('Content-Type: application/json');
			echo json_encode(array("data" => "Método não Permitido","status" => 500));
			exit;
		}

	}


}

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$Cidades = new CidadesController();

$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : $request->action;

switch($action){
	case 'all':
		$Cidades->checkMethod("GET");
		$Cidades->all();
	break;
	case 'create':

		$Cidades->checkMethod("POST");
		$Cidades->create();

	break;
	case 'delete':

		$Cidades->checkMethod("DELETE");
		$Cidades->delete();

	break;
	case 'update':

		$Cidades->checkMethod("PUT");
		$Cidades->update();

	break;
	case 'pesquisar':

		$Cidades->checkMethod("GET");
		$Cidades->pesquisar();

	break;
	default:
		echo json_encode(array());
	break;
}


?>