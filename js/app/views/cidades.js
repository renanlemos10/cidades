$app.directive("crudCidades",["VIEW","CidadeService","$rootScope","$timeout","AppService",function(VIEW,CidadeService,$rootScope,$timeout,AppService){
	return{
		restrict: "E",
		scope: true,
		replace: true,
		templateUrl: VIEW+"index.html",
		link: function($scope,element,attrs,model){

			var modal        = null
			var modalMessage = null

			$scope.window = {
				modal:{
					open: function(dataItem){

						$scope.reloadGrid = false

						if(angular.isDefined(dataItem)){
							$scope.dataItem = angular.copy(dataItem)
							$scope.dataItem.actionForm = "update"
						}else{
							$scope.initDataItem()
						}

						modal.dialog("open")
					},
					close: function(){

						modal.dialog("close")
					}
				},
				info: {
					open: function(){
						modalMessage.dialog("open")
					},
					close: function(){
						modalMessage.dialog("close")
					}
				}
			}
			$scope.pesquisar = function(){

				$rootScope.isLoading = true
				var q = CidadeService.pesquisar($scope.descricao)

				$timeout(function(){
						
						q.then(function(response){

							$rootScope.isLoading = false
							var response     = response.data

							switch(response.status){
								case 1:
								   $scope.response.data = response.data 
								break	
								default:
									
									$scope.messages = {
										data: "Houve um erro: "+response.data,
										class: "danger"
									}
									$scope.window.info.open()
								
								break						

							}

						},function(response){

							$rootScope.isLoading = false

							$scope.messages = {
								data: "Houve um erro interno",
								class: "danger"
							}
							$scope.window.info.open()

						})

					},500)	


			}
			$scope.salvar = function(){

				try{

					$rootScope.isLoading = true
					if($scope.dataItem.actionForm == "create"){
						var q = CidadeService.create($scope.dataItem)
					}else{
						var q = CidadeService.update($scope.dataItem)
					}
					
					$timeout(function(){
						
						q.then(function(response){

							$rootScope.isLoading = false
							var response     = response.data

							switch(response.status){
								case 1:
								    if(response.data){
										$scope.messages = {
											data: "Cidade salva com Sucesso.",
											class: "success"
										}
										$scope.reloadGrid = true
										if($scope.dataItem.actionForm == "create"){
											$scope.initDataItem()
										}
									}else{
										$scope.messages = {
											data: "Houve um erro ao salvar a cidade.",
											class: "danger"
										}
									}	
									$scope.window.info.open()
								break	
								case 0:

									$scope.messages = {
										data: AppService.getErrors(response.data),
										class: "danger"
									}
									$scope.window.info.open()

								break	
								default:
									
									$scope.messages = {
										data: "Houve um erro: "+response.data,
										class: "danger"
									}
									$scope.window.info.open()
								
								break						

							}

						},function(response){

							$rootScope.isLoading = false

							$scope.messages = {
								data: "Houve um erro interno",
								class: "danger"
							}
							$scope.window.info.open()

						})

					},500)	

				}catch(e){


					$rootScope.isLoading = false
					$scope.messages = {
						data: "Houve um erro: "+e.message,
						class: "danger"
					}
					$scope.window.info.open()

				}


			}
			$scope.deletar = function(id,index){

				try{

					if(confirm("Deseja deletar o registro "+id+" ?")){
					
						$rootScope.isLoading = true

						var q = CidadeService.delete(id)
							
						$timeout(function(){
							
							q.then(function(response){

								$rootScope.isLoading = false
								var response     = response.data

								switch(response.status){
									case 1:
									    if(response.data){
											$scope.messages = {
												data: "Cidade deletada com Sucesso.",
												class: "success"
											}
											$scope.response.data.splice(index,1)
										}else{
											$scope.messages = {
												data: "Houve um erro ao salvar a cidade.",
												class: "danger"
											}
										}	
										$scope.window.info.open()
									break	
									case 0:
									break	
									default:
										
										$scope.messages = {
											data: "Houve um erro: "+response.data,
											class: "danger"
										}
										$scope.window.info.open()
									
									break						

								}

							},function(response){

								$rootScope.isLoading = false

								$scope.messages = {
									data: "Houve um erro interno",
									class: "danger"
								}
								$scope.window.info.open()

							})

						},500)

					}		

				}catch(e){

					$rootScope.isLoading = false
					$scope.messages = {
						data: "Houve um erro: "+e.message,
						class: "danger"
					}
					$scope.window.info.open()

				}


			}
			$scope.get = function(){
				
				try{

					$rootScope.isLoading = true
					var cidades = CidadeService.all()

					$timeout(function(){
					
						cidades.then(function(response){

							$rootScope.isLoading = false
							$scope.response = response.data


						},function(response){

							$rootScope.isLoading = false
							$scope.response = response.data

						})

					},500)	

				}catch(e){
					$scope.response = response.data
				}

			}
			$scope.initDataItem = function(){

				$scope.dataItem = {
					descricao: "",
					cidade_id: "",
					actionForm: "create"
				}

			}
			var configModal = function(){

				modal = $("#modal").dialog({
					width: "350",
					height: "250",
					modal: true,
					title: "Cidades",
					close: function(){

						if($scope.reloadGrid){
							$scope.get()
						}
						$scope.openModal = false
					},
					open: function(){
						$scope.openModal = true
					}
				})

				modal.dialog("close")

				modalMessage = $("#modalMessage").dialog({
					width: "350",
					modal: true,
					title: "Atenção"
				})

				modalMessage.dialog("close")

			}
			var init = function(){

				configModal()
				$scope.get()
			}
			init()


		}
	}
}])