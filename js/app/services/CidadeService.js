$app.service("CidadeService",["$http","BASE_API",function($http,BASE_API){

	this.all = function(){

		return $http.get(BASE_API+"CidadesController.php",{params:{action:"all"}})

	}
	this.create = function(dataItem){

		dataItem.action = "create"	

		return $http.post(BASE_API+"CidadesController.php",dataItem)

	}
	this.update = function(dataItem){

		dataItem.action = "update"	

		return $http.put(BASE_API+"CidadesController.php",dataItem)

	}
	this.delete = function(id){

		return $http.delete(BASE_API+"CidadesController.php",{params:{id:id,action:"delete"}})

	}
	this.pesquisar = function(descricao){

		return $http.get(BASE_API+"CidadesController.php",{params:{descricao:descricao,action:"pesquisar"}})

	}

}])