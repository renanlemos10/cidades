$app.directive("comboEstados",["VIEW","EstadoService","$rootScope","$timeout",function(VIEW,EstadoService,$rootScope,$timeout){
	return{
		restrict: "A",
		scope: true,
    	link: function($scope,element,attrs,model){

    		$scope.get = function(){
				
				try{

					var estados = EstadoService.all()

					//$timeout(function(){
					
						estados.then(function(response){

							//$rootScope.isLoading = false
							$scope.response = response.data



						},function(response){

							//$rootScope.isLoading = false
							$scope.response = response.data

						})

					//},500)	

				}catch(e){
					$scope.response = response.data
				}

			}
			
			var init = function(){

				$scope.get()
			}
			init()


		}
	}
}])