$app.directive('moduloLoader',function($compile){

  var template = '<div class="modulo-loader modulo-loader-form">';
  template     += '<div class="loader">';
  template     += '<img src="images/loaders/loader.gif" />';
  template     += '<strong data-ng-show="message.length > 0" class="message">{{message}}</strong>';
  template     += '</div>';
  template     += '<div class="overlay"></div> ';
  template     += '</div>';

  return{
    restrict: 'E',
    scope:{
      message: "@message"
    }, 
    template: template,
    link: function(scope,element,attrs,model){
      
    }
  }
});