$app.directive("ngHtml",function(){
	return{
		restrict: "AE",
		scope:{
			ngHtml: "=ngHtml"
		},
		link: function($scope,element,attrs,model){

			$scope.$watch("ngHtml",function(value){

				if(angular.isDefined(value)){
					element.html(value)
				}

			})

		}
	}
})