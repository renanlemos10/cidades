<!DOCTYPE html>
<html lang="pt-BR" data-ng-app="AppCidades">
<head>
	<title>CRUD de Cidades</title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/vendor/jquery-ui.min.css"></link>
	<link rel="stylesheet" type="text/css" href="css/estilos/estilo.css"></link>
	<link rel="stylesheet" type="text/css" href="css/estilos/loader.css"></link>

	<script type="text/javascript" src="js/vendor/jquery.min.js"></script>
	<script type="text/javascript" src="js/vendor/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/vendor/angular.min.js"></script>
	<script type="text/javascript" src="js/app/app.js"></script>
	<script type="text/javascript" src="js/app/services/CidadeService.js"></script>
	<script type="text/javascript" src="js/app/services/EstadoService.js"></script>
	<script type="text/javascript" src="js/app/services/AppService.js"></script>
	<script type="text/javascript" src="js/app/views/cidades.js"></script>
	<script type="text/javascript" src="js/app/modulos/loaders.js"></script>
	<script type="text/javascript" src="js/app/modulos/combos.js"></script>
	<script type="text/javascript" src="js/app/modulos/helpers.js"></script>


</head>
<body>
	<modulo-loader data-ng-show="isLoading"></modulo-loader>
	<crud-cidades></crud-cidades>
</body>
</html>